
export interface Namespace {
    id: string,
    name: string,
    role: string
}

export interface NamespaceUser {
    id: string,
    username: string,
    role: string
}

export interface NamespaceInput {
    id?: string,
    name: string
}


export interface NamespaceService {
    addUserToNamespace: (namespaceId: string, user: NamespaceUser) => Promise<void>
    removeUserFromNamespace: (namespaceId: string, userId: string) => Promise<void>
    get: (namespaceId: string) => Promise<Namespace | null>,
    listNamespaces: () => Promise<Namespace[]>,
    listUserNamespaces: (userId: string) => Promise<Namespace[]>,
    listNamespaceUsers: (namespaceId: string, searchString: string | null) => Promise<NamespaceUser[]>
    createOrUpdate: (namespace: NamespaceInput) => Promise<string>
}