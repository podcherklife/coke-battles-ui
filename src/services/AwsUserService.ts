import { User, UserService } from "./UserService";


export const awsUserService: (idTokenProvider: () => Promise<string>, awsEndpoint: string) => UserService = (tokenProvider, awsEndpoint) => {
    return {
        get: () => { throw "not implemented" },
        list: (searchString: string) => tokenProvider().then(token => listUsersFromAws(searchString, token, awsEndpoint))
    };
}

function listUsersFromAws(searchString: string, idToken: string, awsEndpoint: string) {
    return new Promise<User[]>((resolve, reject) =>
        fetch(`${awsEndpoint}/users/list?${new URLSearchParams({ searchString })}`, {
            method: "GET",
            headers: {
                Authorization: `Bearer ${idToken}`
            },
        }).then(result => result.ok
            ? result.json().then(items => items as User[])
                .then(resolve)
            : result.text().then(reject))
            .catch(err => { reject(err) })
    );
}