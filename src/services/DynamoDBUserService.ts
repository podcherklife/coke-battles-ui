import AWS from "aws-sdk";
import { DocumentClient } from "aws-sdk/clients/dynamodb";
import { User, UserService } from "./UserService";

export const dynamoDBUserService: (dynamoDB: AWS.DynamoDB.DocumentClient) => UserService = (dynamoDB) => {
    return {
        claim: () => {
            throw "not implemented"
        },
        get: (userId: string) => {
            return get(dynamoDB, userId);
        },
        list: (searchString) => {
            return list(dynamoDB, searchString);
        }
    }
}


async function get(dynamo: AWS.DynamoDB.DocumentClient, userId: string) {
    return dynamo.get({
        TableName: "Users",
        Key: {
            userId
        }
    }).promise().then((result) => {
        return result.Item == null ? null : result.Item as User;
    });
}

async function list(dynamo: AWS.DynamoDB.DocumentClient, searchString: string) {
    const expectedResultSize = 5;
    const scanChunkSize = 5;
    async function scan(searchString: string, startKey: DocumentClient.ScanOutput['LastEvaluatedKey']) {
        return dynamo.scan({
            ExpressionAttributeValues: {
                ":searchString": searchString,
                ":byId": true,
            },
            ExclusiveStartKey: startKey,
            TableName: "Users",
            Limit: scanChunkSize,
            FilterExpression: "contains(username, :searchString) and byId = :byId"
        }).promise();
    };

    let result: User[] = [];
    let lastEvaluatedKey: DocumentClient.ScanOutput['LastEvaluatedKey'] = undefined;
    do {
        const scanResult: DocumentClient.ScanOutput = await scan(searchString, lastEvaluatedKey);
        if (scanResult.Items != null) {
            result = result.concat(scanResult.Items.map(e => e as User))
        }
        lastEvaluatedKey = scanResult.LastEvaluatedKey;
    } while (result.length < expectedResultSize && lastEvaluatedKey != null)
    return result;
}