import { Namespace, NamespaceInput, NamespaceService, NamespaceUser } from "./NamespaceService";

export const awsNamespaceService: (idTokenProvider: () => Promise<string>, awsEndpoint: string) => NamespaceService = (tokenProvider, awsEndpoint) => {
    return {
        get: (namespaceId: string) => getNamespaceFromAws(namespaceId, awsEndpoint),
        createOrUpdate: (namespace: NamespaceInput) => tokenProvider().then(token => createOrUpdateNamespaceOnAws(awsEndpoint, token, namespace)),
        listNamespaces: () => { throw "not implemented" },
        listUserNamespaces: (userId: string) => tokenProvider().then(token => listUserNamespacesFromAws(awsEndpoint, token, userId)),
        listNamespaceUsers: (namespaceId: string, searchString: string | null) => tokenProvider().then(token => listNamespaceUsersOnAws(awsEndpoint, token, namespaceId, searchString)),
        addUserToNamespace: (namespaceId: string, user: NamespaceUser) => tokenProvider().then(token => addUserToNamespace(awsEndpoint, token, namespaceId, user)),
        removeUserFromNamespace: (namespaceId: string, userId: string) => tokenProvider().then(token => removeUserFromNamespace(awsEndpoint, token, namespaceId, userId))
    };
}

function getNamespaceFromAws(namespaceId: string, awsEndpoint: string) {
    return new Promise<Namespace | null>((resolve, reject) =>
        fetch(`${awsEndpoint}/namespaces?${new URLSearchParams({ namespaceId })}`, {
            method: "GET",
        }).then(result => result.ok
            ? result.json().then(items => items as Namespace)
                .then(resolve)
            : result.text().then(reject))
            .catch(err => { reject(err) })
    );
}

function listUserNamespacesFromAws(awsEndpoint: string, idToken: string, userId: string): Promise<Namespace[]> {
    return new Promise((resolve, reject) => fetch(`${awsEndpoint}/usersAndNamespaces?${new URLSearchParams({ userId })}`, {
        method: "GET",
        headers: {
            "Content-Type": 'application/json',
            Authorization: `Bearer ${idToken}`
        },
    }).then(result => result.ok
        ? result.json().then(data => resolve(data as Namespace[])) : result.text().then(reject)));
};

function listNamespaceUsersOnAws(awsEndpoint: string, token: string, namespaceId: string, searchString: string | null): Promise<NamespaceUser[]> {
    return new Promise((resolve, reject) => fetch(`${awsEndpoint}/usersAndNamespaces/users?${new URLSearchParams({ namespaceId, searchString: searchString || "" })}`, {
        method: "GET",
        headers: {
            "Content-Type": 'application/json',
            Authorization: `Bearer ${token}`
        },
    }).then(result => result.ok
        ? result.json().then(data => resolve(data as NamespaceUser[])) : result.text().then(reject)));
}

function createOrUpdateNamespaceOnAws(awsEndpoint: string, idToken: string, namespace: NamespaceInput) {
    return new Promise<string>((resolve, reject) => fetch(`${awsEndpoint}/namespaces`, {
        method: "POST",
        headers: {
            "Content-Type": 'application/json',
            Authorization: `Bearer ${idToken}`
        },
        body: JSON.stringify(namespace)
    }).then(result => result.ok
        ? result.json().then(data => resolve(data as string)) : result.text().then(reject)));
}

function addUserToNamespace(awsEndpoint: string, idToken: string, namespaceId: string, user: NamespaceUser): Promise<void> {
    const params = new URLSearchParams({ namespaceId, userId: user.id });
    return new Promise<void>((resolve, reject) => fetch(`${awsEndpoint}/usersAndNamespaces/addUser?${params}`, {
        method: "POST",
        headers: {
            "Content-Type": 'application/json',
            Authorization: `Bearer ${idToken}`
        },
    }).then((result) => result.ok ? resolve() : result.text().then(reject))
        .catch(reject));
}

function removeUserFromNamespace(awsEndpoint: string, idToken: string, namespaceId: string, userId: string): Promise<void> {
    const params = new URLSearchParams({ namespaceId, userId });
    return new Promise<void>((resolve, reject) => fetch(`${awsEndpoint}/usersAndNamespaces/?${params}`, {
        method: "DELETE",
        headers: {
            "Content-Type": 'application/json',
            Authorization: `Bearer ${idToken}`
        },
    }).then((result) => result.ok ? resolve() : result.text().then(reject))
        .catch(reject));
}



