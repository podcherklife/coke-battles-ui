export interface CokeBattle {
    id: string,
    description: string,
    creator: {
        id: string,
        username: string
    },
    creationDate: number,
    expectedResolutionDate: number | null
    sides: {
        description: string,
        participants: {
            id: string,
            username: string
        }[]
    }[],
    namespace: {
        id: string
    }
}

export interface CokeBattleRepository {
    saveOrUpdate: (battle: CokeBattle) => Promise<void>,
    delete: (battle: CokeBattle) => Promise<void>,
    listAll: (namespaceId: string) => Promise<CokeBattle[]>
}