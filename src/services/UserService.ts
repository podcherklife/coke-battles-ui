
export interface User {
    id: string,
    username: string,
    phantom: boolean
}


export interface UserService {
    get: (identityId: string) => Promise<User | null>,
    list: (searchString: string) => Promise<User[]>
}