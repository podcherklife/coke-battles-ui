import { awsRepository } from "./AwsBattleService";
import { awsNamespaceService } from "./AwsNamespaceService";
import { awsUserService } from "./AwsUserService";
import { CokeBattleRepository as CokeBattleService } from "./CokeBattleService";
import { dynamoRepository } from "./DynamoDBBattleService";
import { dynamoDBUserService } from "./DynamoDBUserService";
import { localDynamoDBConfig } from "./LocalDynamoDBConfig";
import { NamespaceService } from "./NamespaceService";
import { UserService } from "./UserService";

interface ServiceProvider {
    userService: UserService,
    battleService: CokeBattleService,
    namespaceService: NamespaceService,
}

export const remoteDynamoDB: (dynamoDB: AWS.DynamoDB.DocumentClient) => ServiceProvider = (dynamoDB) => {
    return {
        userService: dynamoDBUserService(dynamoDB),
        battleService: dynamoRepository(dynamoDB),
        namespaceService: null as any as NamespaceService
    };
}

export const localDynamoDB: () => ServiceProvider = () => remoteDynamoDB(localDynamoDBConfig);

export const noop: () => ServiceProvider = () => {
    function nope(): any {
        throw "Noop implementation";
    }
    return {
        battleService: {
            delete: nope,
            listAll: nope,
            saveOrUpdate: nope
        },
        userService: {
            get: nope,
            list: nope,
        },
        namespaceService: {
            createOrUpdate: nope,
            get: nope,
            listNamespaces: nope,
            listUserNamespaces: nope,
            listNamespaceUsers: nope,
            create: nope,
            addUserToNamespace: nope,
            removeUserFromNamespace: nope,
        },
    };
}

export const aws: (idTokenProvider: () => Promise<string>, awsEndpoint: string) => ServiceProvider = (idTokenProvider, awsEndpoint) => {
    return {
        userService: awsUserService(idTokenProvider, awsEndpoint),
        battleService: awsRepository(idTokenProvider, awsEndpoint),
        namespaceService: awsNamespaceService(idTokenProvider, awsEndpoint),
    }
}