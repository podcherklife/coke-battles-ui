import AWS from "aws-sdk";

export const localDynamoDBConfig = new AWS.DynamoDB.DocumentClient({
    accessKeyId: "asd",
    endpoint: "http://localhost:8000",
    region: "us-west-2",
    credentials: {
        accessKeyId: "key",
        secretAccessKey: "secretKey"
    }
});