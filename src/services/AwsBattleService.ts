import { CokeBattle, CokeBattleRepository } from "./CokeBattleService";

export const awsRepository: (idTokenProvider: () => Promise<string>, awsEndpoint: string) => CokeBattleRepository = (idTokenProvider, awsEndpoint) => {
    return {
        saveOrUpdate: cokeBattle => idTokenProvider().then(idToken => saveCokeBattleToAws(cokeBattle, idToken, awsEndpoint)),
        delete: cokeBattle => idTokenProvider().then(idToken => deleteCokeBattleFromAws(cokeBattle, idToken, awsEndpoint)),
        listAll: (namespaceId: string) => listAllCokeBattlesFromAws(namespaceId, awsEndpoint)
    }
}

function deleteCokeBattleFromAws(cokeBattle: CokeBattle, idToken: string, awsEndpoint: string) {
    return new Promise<void>((resolve, reject) => {
        fetch(`${awsEndpoint}/battles`, {
            method: "DELETE",
            headers: {
                "Content-Type": 'application/json',
                Authorization: `Bearer ${idToken}`
            },
            body: JSON.stringify({
                battleId: cokeBattle.id,
                namespaceId: cokeBattle.namespace.id
            })
        }).then(result => result.ok
            ? result.json().then(resolve)
            : result.text().then(reject))
            .catch(err => { reject(err.message) });
    });
}
function listAllCokeBattlesFromAws(namespaceId: string, awsEndpoint: string) {
    return new Promise<CokeBattle[]>((resolve, reject) =>
        fetch(`${awsEndpoint}/battles/list?${new URLSearchParams({ "namespaceId": namespaceId })}`, {
            method: "GET"
        }).then(result => {
            if (result.ok) {
                result.json().then(items => {
                    resolve((items as CokeBattle[]).map(e => e.sides == null ? { ...e, sides: [] } : e))
                })
            } else {
                result.text().then(reject);
            }
        }).catch(err => reject(err.message)));
}

function saveCokeBattleToAws(cokeBattle: CokeBattle, idToken: string, awsEndpoint: string) {
    return new Promise<void>((resolve, reject) => {
        fetch(`${awsEndpoint}/battles`, {
            method: "POST",
            headers: {
                "Content-Type": 'application/json',
                Authorization: `Bearer ${idToken}`
            },
            body: JSON.stringify(cokeBattle)
        }).then(result => {
            if (result.ok) {
                result.json().then(resolve)
            } else {
                result.text().then(reject)
            }
        }).catch(err => reject(err.message))
    });
}