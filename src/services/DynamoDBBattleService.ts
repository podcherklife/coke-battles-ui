import * as AWS from "aws-sdk";
import { CokeBattle, CokeBattleRepository } from "./CokeBattleService";

export const dynamoRepository: (dynamoDB: AWS.DynamoDB.DocumentClient) => CokeBattleRepository = (dynamoDB) => {
    return {
        saveOrUpdate: cokeBattle => saveCokeBattleToDb(cokeBattle, dynamoDB),
        delete: cokeBattle => deleteCokeBattleFromDynamo(cokeBattle, dynamoDB),
        listAll: () => listAllCokeBattlesFromDynamo(dynamoDB)
    }
}


function listAllCokeBattlesFromDynamo(dynamo: AWS.DynamoDB.DocumentClient) {
    return new Promise<CokeBattle[]>((resolve, reject) => {
        dynamo.scan({
            TableName: "CokeBattles",
            Limit: 10
        }).send((err, data) => {
            if (err) {
                reject(err.message);
            } else {
                resolve((data.Items as CokeBattle[])
                    .map(e => { return { ...e, transient: false, editing: false, sides: e.sides || [] } }));
            }
        })
    });
}

function saveCokeBattleToDb(battle: CokeBattle, dynamo: AWS.DynamoDB.DocumentClient) {
    return new Promise<void>((resolve, reject) =>
        dynamo.put({
            TableName: "CokeBattles",
            Item: {
                id: battle.id,
                description: battle.description,
                expectedResolutionDate: battle.expectedResolutionDate,
                creationDate: battle.creationDate,
                creator: battle.creator,
                sides: battle.sides
            }
        }).send((err, data) => {
            if (err) {
                reject(err.message);
            } else {
                resolve();
            }
        })
    )
}


function deleteCokeBattleFromDynamo(battle: CokeBattle, dynamo: AWS.DynamoDB.DocumentClient) {
    return new Promise<void>((resolve, reject) =>
        dynamo.delete({
            TableName: "CokeBattles",
            Key: {
                id: battle.id
            }
        }).send((err, data) => {
            if (err) {
                reject(err.message);
            } else {
                resolve();
            }
        })
    )
}