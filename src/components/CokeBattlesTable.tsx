import { DateTimeFormatter, Instant, ZonedDateTime, ZoneId } from "js-joda";
import React, { useContext } from "react";
import { useParams } from "react-router-dom";
import SemanticDatepicker from "react-semantic-ui-datepickers";
import { Button, ButtonGroup, Form, Header, Icon, Label, List, Loader, Message, Popup, SemanticCOLORS, Table, TextArea } from "semantic-ui-react";
import { CokeBattle } from '../services/CokeBattleService';
import { Namespace } from "../services/NamespaceService";
import { ServiceContext } from "./App";
import { ProfileContext } from "./Auth";
import { NamespaceReference } from "./NamespaceEditor";
import { UserSelector } from "./UserSelector";

interface CokeBattleRowState extends CokeBattle {
    editing: boolean,
    transient: boolean,
}

export function CokeBattlesTable() {
    const [battles, setBattles] = React.useState<CokeBattleRowState[]>([]);
    const [currentNamespace, setCurrentNamespace] = React.useState<Namespace | null>(null);
    const [loadingBattles, setLoadingBattles] = React.useState(false);
    const [loadingNamespace, setLoadingNamespace] = React.useState(false);
    const [errorMessage, setError] = React.useState<any>("");
    const [refreshRequestToken, updateRefreshTableToken] = React.useReducer((num) => num + 1, 0);
    const auth = useContext(ProfileContext);
    const namespaceIdParam = useParams<{ "namespace": string }>().namespace;


    React.useEffect(() => {
        setLoadingNamespace(true);
        serviceProvider.namespaceService
            .get(namespaceIdParam)
            .then(namespace => {
                if (namespace == null) {
                    setError("Namespace not found");
                } else {
                    setCurrentNamespace(namespace);
                }
            })
            .catch(setError)
            .finally(() => setLoadingNamespace(false))
    }, [namespaceIdParam]);

    const serviceProvider = React.useContext(ServiceContext);

    const refreshTable = (currentNamespaceId?: string) => {
        if (!currentNamespaceId) {
            return;
        }
        setLoadingBattles(true);
        serviceProvider.battleService.listAll(currentNamespaceId)
            .then(items => items.map(e => { return { ...e, transient: false, editing: false }; }))
            .then(items => setBattles(items))
            .catch(setError)
            .finally(() => setLoadingBattles(false));
    }
    React.useEffect(() => { refreshTable(currentNamespace?.id) }, [currentNamespace?.id, refreshRequestToken]);


    return <>
        {errorMessage && <>
            <Message error onDismiss={() => setError("")}>
                <Message.Header>
                    Oh no, something is wrong!
                </Message.Header>
                <Message.Content>
                    {errorMessage instanceof Error ? errorMessage.message : errorMessage}
                </Message.Content>
            </Message>
        </>}

        <Header as='h1'>Coke Battles {loadingNamespace || currentNamespace == null
            ? <Loader active size="mini" inline />
            : <>({<NamespaceReference name={currentNamespace.name} namespaceId={currentNamespace.id} allowEdit={true} />})</>}
        </Header>
        <Table celled>
            <Table.Header>
                <Table.Row>
                    <Table.HeaderCell width="1">Key</Table.HeaderCell>
                    <Table.HeaderCell width="4">Description</Table.HeaderCell>
                    <Table.HeaderCell width="4">Participants</Table.HeaderCell>
                    <Table.HeaderCell width="2">Resolution date</Table.HeaderCell>
                    <Table.HeaderCell width="3">Created</Table.HeaderCell>
                    <Table.HeaderCell width="1"></Table.HeaderCell>
                </Table.Row>
            </Table.Header>
            <Table.Body>
                {(loadingBattles || loadingNamespace) &&
                    <Table.Row>
                        <Table.Cell colSpan="6">
                            <Loader active inline="centered" />
                        </Table.Cell>
                    </Table.Row>
                }
                {battles.map(battle => <CokeBattleRow
                    disabled={loadingBattles}
                    key={battle.id}
                    battle={battle}
                    handleDelete={(deletedBattle) => {
                        setLoadingBattles(true);
                        serviceProvider.battleService.delete(deletedBattle)
                            .then(() => setBattles(battles.filter(e => e.id !== deletedBattle.id)))
                            .then(() => updateRefreshTableToken())
                            .catch((err) => setError(err))
                            .finally(() => setLoadingBattles(false));
                    }}
                    handleSave={(modifiedBattle) => {
                        setLoadingBattles(true);
                        let saveState = { ...modifiedBattle };
                        if (modifiedBattle.transient) {
                            delete (saveState as any)['id'];
                        }
                        serviceProvider.battleService.saveOrUpdate(saveState)
                            .then(() => setBattles(battles.map(e => e.id === modifiedBattle.id ? { ...e, ...modifiedBattle, editing: false, transient: false } : e)))
                            .then(() => updateRefreshTableToken())
                            .catch((err) => setError(err))
                            .finally(() => setLoadingBattles(false));
                    }}
                    handleCancel={(noLongerEditedBattle) => {
                        if (noLongerEditedBattle.transient) {
                            setBattles(battles.filter(e => e !== noLongerEditedBattle));
                        } else {
                            setBattles(battles.map(e => e.id === noLongerEditedBattle.id ? { ...e, editing: false } : e));
                        }
                    }}
                    handleStartEdit={(noLongerEditedBattle) => setBattles(battles.map(e => e.id === noLongerEditedBattle.id ? { ...e, editing: true } : e))}
                />)}
                {auth.isAuthenticated && currentNamespace?.id &&
                    < CokeBattleAddRow
                        disabled={loadingBattles}
                        onAdd={() => setBattles(battles.concat({
                            creationDate: Instant.now().epochSecond(),
                            editing: true,
                            transient: true,
                            description: "",
                            id: `${new Date().getTime()}_${Math.random()}`,
                            creator: {
                                id: auth.profile!.userId,
                                username: auth.profile!.username
                            },
                            sides: [],
                            namespace: {
                                id: currentNamespace.id
                            },
                            expectedResolutionDate: null
                        }))} />
                }
            </Table.Body>
        </Table>
    </>

}

function CokeBattleAddRow(props: { onAdd: () => void, disabled: boolean }) {
    return <Table.Row>
        <Table.Cell colSpan="6">
            <Button icon positive
                onClick={props.onAdd}
                disabled={props.disabled}>
                <Icon name="add" ></Icon>
            </Button>
        </Table.Cell>
    </Table.Row>
}

function CokeBattleRow(props: {
    battle: CokeBattleRowState,
    disabled: boolean,
    handleSave: (battle: CokeBattleRowState) => void,
    handleDelete: (battle: CokeBattleRowState) => void,
    handleCancel: (battle: CokeBattleRowState) => void,
    handleStartEdit: (battle: CokeBattleRowState) => void
}) {
    if (props.battle.editing) {
        return <CokeBattleEditRow
            disabled={props.disabled}
            battle={props.battle}
            handleSave={(modifiedBattle) => props.handleSave(modifiedBattle)}
            handleCancel={() => props.handleCancel(props.battle)}
            handleDelete={() => props.handleDelete(props.battle)} />
    } else {
        return <CokeBattleDisplayRow battle={props.battle} disabled={props.disabled} onEdit={() => props.handleStartEdit(props.battle)} />
    }
}

function CokeBattleDisplayRow(props: { battle: CokeBattleRowState, disabled: boolean, onEdit: () => void }) {
    const auth = useContext(ProfileContext);
    return <Table.Row>
        <Table.Cell>
            <CokeBattleKey battle={props.battle} short />
        </Table.Cell>
        <Table.Cell>{props.battle.description}</Table.Cell>
        <Table.Cell>
            <CokeBattleDisplaySides sides={props.battle.sides} />
        </Table.Cell>
        <Table.Cell>
            <CokeBattleDate date={props.battle.expectedResolutionDate || undefined} />
        </Table.Cell>
        <Table.Cell>
            Created on <CokeBattleDate date={props.battle.creationDate} /> by
            <CokeBattleUser username={props.battle.creator.username} />
        </Table.Cell>
        <Table.Cell>
            {auth.isAuthenticated && auth.profile!.userId == props.battle.creator.id &&
                <Button icon onClick={props.onEdit} disabled={props.disabled}>
                    <Icon name="edit"></Icon>
                </Button>
            }
        </Table.Cell>
    </Table.Row>
}

function CokeBattleEditRow(props: {
    battle: CokeBattleRowState,
    disabled: boolean,
    handleSave: (battle: CokeBattleRowState) => void,
    handleDelete: () => void,
    handleCancel: () => void
}) {
    const [modifiedBattle, setModifiedBattle] = React.useState(props.battle);
    return <Table.Row>
        <Table.Cell colSpan="6">
            <Form>
                <Form.Group inline>
                    <Form.Field>
                        <label>Key:</label>
                        <CokeBattleKey battle={props.battle} short={true} />
                    </Form.Field>
                    {modifiedBattle.creator &&
                        <Form.Field>
                            <label>Created by:</label>
                            <CokeBattleUser username={modifiedBattle.creator.username} />
                        </Form.Field>
                    }
                    {modifiedBattle.creationDate &&
                        <Form.Field>
                            <label>Created on:</label>
                            {` ${formatCokeBattleDate(modifiedBattle.creationDate)}`}
                        </Form.Field>
                    }
                    <CokeBattleEditRowButtons
                        onCancel={props.handleCancel}
                        onSave={() => props.handleSave(modifiedBattle)}
                        onDelete={props.handleDelete}
                        disabled={props.disabled}
                        showDelete={!props.battle.transient}
                    />
                </Form.Group>
                <Form.Field>
                    <label>Description:</label>
                    <TextArea
                        disabled={props.disabled}
                        placeholder="Description"
                        defaultValue={modifiedBattle.description}
                        onChange={(event, data) => setModifiedBattle({ ...modifiedBattle, description: data.value as string })}
                    />
                    <label>Participating sides:</label>
                    <CokeBattleEditSides
                        namespace={props.battle.namespace.id}
                        sides={modifiedBattle.sides}
                        onSidesModified={(sides) => setModifiedBattle({ ...modifiedBattle, sides })} />
                </Form.Field>
                <Form.Field>
                    <label>Expected resolution date:</label>
                    <SemanticDatepicker
                        disabled={props.disabled}
                        value={modifiedBattle.expectedResolutionDate ? new Date(modifiedBattle.expectedResolutionDate * 1000) : null}
                        onChange={(event, data) => setModifiedBattle({
                            ...modifiedBattle,
                            expectedResolutionDate: data.value ? Math.floor((data.value as Date).getTime() / 1000) : null
                        })} />
                </Form.Field>
                <CokeBattleEditRowButtons
                    onCancel={props.handleCancel}
                    onSave={() => props.handleSave(modifiedBattle)}
                    onDelete={props.handleDelete}
                    disabled={props.disabled}
                    showDelete={!props.battle.transient}
                />
            </Form>
        </Table.Cell>
    </Table.Row >
}

function CokeBattleEditRowButtons(props: { onCancel: () => void, onSave: () => void, onDelete: () => void, disabled: boolean, showDelete: boolean }) {
    return <ButtonGroup>
        <Button icon onClick={() => props.onCancel()} disabled={props.disabled}>
            <Icon name="cancel" /> Cancel
        </Button>
        <Button icon primary onClick={() => props.onSave()} disabled={props.disabled}>
            <Icon name="save" /> Save
        </Button>
        {props.showDelete &&
            <Button negative icon onClick={() => props.onDelete()} disabled={props.disabled}>
                <Icon name="trash" /> Delete
            </Button>
        }
    </ButtonGroup>
}

function CokeBattleDisplaySides(props: { sides: CokeBattle['sides'] }) {
    let colors: SemanticCOLORS[] = ["blue", "red", "green", "purple"];
    return <>
        <List>
            {props.sides.map((side, index) => {
                return <List.Item key={index}>
                    <Popup
                        disabled={side.description.length == 0}
                        content={side.description}
                        trigger={
                            <div>
                                <Label>
                                    <div>
                                        {side.participants.map(participant => {
                                            return <Label as="a" color={colors[index]} key={participant.id} >
                                                {participant.username}
                                            </Label>
                                        })}
                                    </div>
                                </Label>
                            </div>
                        } />
                </List.Item>
            })
            }
        </List>
    </>;
}

function CokeBattleEditSides(props: { namespace: CokeBattle['namespace']['id'], sides: CokeBattle['sides'], onSidesModified: (sides: CokeBattle['sides']) => void }) {
    return <>
        <List divided>
            <List.Item>
                <Form.Button onClick={() => props.onSidesModified(props.sides.concat({ description: "", participants: [] }))} icon positive>
                    <Icon name="add" /> Add side
                </Form.Button>
            </List.Item>
            {props.sides.map((side, index) =>
                <List.Item key={index}>
                    <CokeBattleEditSide
                        namespace={props.namespace}
                        side={side}
                        onUpdate={(modifiedSide) => props.onSidesModified(props.sides.map((e, i) => i == index ? modifiedSide : e))}
                        onRemove={() => props.onSidesModified(props.sides.filter((e, oldSideIndex) => oldSideIndex != index))} />
                </List.Item>
            )}
        </List>
    </>;
}

function CokeBattleEditSide(props: {
    namespace: CokeBattle['namespace']['id'],
    side: CokeBattle['sides'][number],
    onRemove: () => void,
    onUpdate: (modifiedSide: CokeBattle['sides'][number]) => void
}) {
    function addParticipant(userToAdd: CokeBattle['sides'][number]['participants'][number]) {
        if (!props.side.participants.find(e => e.id == userToAdd.id)) {
            props.onUpdate({ ...props.side, participants: props.side.participants.concat(userToAdd) })
        }
    }
    function removeParticipant(userToRemove: CokeBattle['sides'][number]['participants'][number]) {
        props.onUpdate({ ...props.side, participants: props.side.participants.filter(e => e.id != userToRemove.id) });
    }
    return <>
        <Form.Group inline>
            <Form.Field>
                <label>Side description:</label>
                <Form.TextArea
                    value={props.side.description}
                    onChange={(evt, data) => props.onUpdate({ ...props.side, description: data.value as (string | null) || "" })} />
            </Form.Field>
            <Form.Field>
                <label></label>
                <Form.Button onClick={() => props.onRemove()} icon negative>
                    <Icon name="delete" /> Remove side
                </Form.Button>
            </Form.Field>
        </Form.Group>
        <Form.Group>
            <Form.Field>
                <label>Participants:</label>
                <List divided>
                    {props.side.participants.length == 0 &&
                        <List.Item key="empty-participants">
                            No participants selected
                        </List.Item>
                    }
                    {props.side.participants.length > 0 && props.side.participants.map(e => {
                        return <List.Item key={e.id}>
                            <Label as="a"
                                onClick={(evt, data) => removeParticipant(e)}>
                                {e.username}
                                <Icon name="delete" />
                            </Label>
                        </List.Item>
                    })}
                    <List.Item key="search-participant">
                        <UserSelector namespace={props.namespace} onSelect={(user) => addParticipant(user)} />
                    </List.Item>
                </List>
            </Form.Field>
        </Form.Group>
    </>;
}


function CokeBattleKey(props: { battle: CokeBattleRowState, short: boolean }) {
    const displayString = props.battle.transient ? "Not generated yet" : `${props.battle.namespace.id}/${props.battle.id}`;
    return <Popup hoverable
        disabled={!props.short}
        content={displayString}
        trigger={
            <Label>
                {props.short ? displayString.substr(0, 9) + "..." : displayString}
            </Label>
        } />
}

export function CokeBattleUser(props: { username: string }) {
    return <Label>
        {props.username}
    </Label>;
}

function CokeBattleDate(props: { date?: number }) {
    return <>
        {props.date && formatCokeBattleDate(props.date)}
    </>;
}
function formatCokeBattleDate(date: number) {
    return ZonedDateTime.ofInstant(Instant.ofEpochSecond(date), ZoneId.of("SYSTEM"))
        .format(DateTimeFormatter.ISO_LOCAL_DATE)
}