import { Auth0Provider, useAuth0 } from "@auth0/auth0-react";
import * as React from "react";
import { config } from "../config";

export interface ProfileContextValue {
    profile?: Profile,
    isLoading: boolean,
    isAuthenticated: boolean,
    loginError?: string,
    doLogin: () => Promise<void>,
    doLogout: () => void
}
export interface Profile {
    userId: string,
    username: string,
    picture: string
}

export const ProfileContext = React.createContext<ProfileContextValue>({
    doLogin: () => { throw new Error('missing ProfileContext wrapping component'); },
    doLogout: () => { throw new Error('missing ProfileContext wrapping component'); },
    isLoading: false,
    isAuthenticated: false
});


interface ProfileProviderState {
    completedAuth0LoginAttempt: boolean,
    loading: boolean,
    error?: string,
    profile?: Profile
}

type ProfileProviderStateAction = {
    type: "startLogin"
} | {
    type: "completeLogin",
    profile: Profile
} | {
    type: "indicateError",
    error: string
} | {
    type: "logout"
}

function profileStateReducer(state: ProfileProviderState, action: ProfileProviderStateAction): ProfileProviderState {
    switch (action.type) {
        case "startLogin": {
            return {
                completedAuth0LoginAttempt: false,
                profile: undefined,
                error: undefined,
                loading: true
            }
        }
        case "completeLogin": {
            return {
                completedAuth0LoginAttempt: true,
                loading: false,
                error: undefined,
                profile: action.profile
            }
        }
        case "logout": {
            return {
                completedAuth0LoginAttempt: false,
                error: undefined,
                profile: undefined,
                loading: false
            }
        }
        case "indicateError": {
            return {
                ...state,
                completedAuth0LoginAttempt: true,
                loading: false,
                error: action.error,
            }
        }
    }
}


export function ProfileProvider(
    props: {
        children?: React.ReactNode;
    }) {

    const [state, dispatch] = React.useReducer(profileStateReducer, { loading: false, completedAuth0LoginAttempt: false });
    const auth = useAuth0();

    const initialValue: ProfileContextValue = {
        isAuthenticated: state.profile != null,
        profile: state.profile,
        isLoading: state.loading,
        loginError: undefined,
        doLogout: () => {
            auth.logout({ logoutParams: { returnTo: window.location.origin, localOnly: true } });
            dispatch({ type: "logout" });
        },
        doLogin: async () => {
            dispatch({ type: "startLogin" });
            return (auth.isAuthenticated ? Promise.resolve()
                : auth.loginWithPopup(undefined, { timeoutInSeconds: 30 }))
                .then(() => {
                    // FIXME: auth.isAuthenticated flag is false right after login
                    return Promise.all([
                        auth.getIdTokenClaims().then(idToken => {
                            if (idToken) {
                                return idToken.__raw;
                            } else {
                                throw new Error("Failed to get token")
                            }
                        }),
                        auth.getAccessTokenSilently().then(accessToken => accessToken)]);
                })
                .then(async ([idToken, accessToken]) => {
                    return fetch(`${config.aws.api}/profile?accessToken=${accessToken}`,
                        {
                            mode: "cors",
                            headers: { Authorization: `Bearer ${idToken}` }
                        })
                        .then(data => data.json()
                            .then(json => {
                                dispatch({
                                    type: "completeLogin",
                                    profile: {
                                        picture: json.picture,
                                        userId: json.id,
                                        username: json.username
                                    }
                                })
                            }))
                })
                .catch((err) => {
                    dispatch({
                        type: "indicateError",
                        error: err
                    })
                })
        }
    }
    if (!state.loading && state.error == null && state.profile == null && auth.isAuthenticated) {
        initialValue.doLogin();
    }
    return <>
        <Auth0Provider
            domain={config.zeroAuth.domain}
            clientId={config.zeroAuth.clientId}
            authorizationParams={{
                redirectUri: window.location.origin
            }}
            cacheLocation="localstorage"
        >
            <ProfileContext.Provider value={initialValue}>
                {props.children}
            </ProfileContext.Provider>
        </Auth0Provider >,
    </>;
}
