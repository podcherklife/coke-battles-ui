import React from "react";
import { Link, useParams } from "react-router-dom";
import { Button, ButtonGroup, Dimmer, Dropdown, Form, FormField, Icon, Input, Loader, Table } from "semantic-ui-react";
import { Namespace, NamespaceUser } from "../services/NamespaceService";
import { ServiceContext } from "./App";
import { CokeBattleUser } from "./CokeBattlesTable";
import { UserSelector } from "./UserSelector";
type EditableNamespace = Omit<Namespace, "role">;

export function NamespaceEditor() {
    const namespaceIdParam = useParams<{ "namespace": string }>().namespace;
    return <NamespaceForm namespaceId={namespaceIdParam} />;
}
function NamespaceForm(props: { namespaceId: string }) {
    const serviceProvider = React.useContext(ServiceContext);

    const [loaded, setLoaded] = React.useState<boolean>(false);
    const [savingInProgress, setSavingInProgress] = React.useState(false);
    const [namespace, setNamespace] = React.useState<EditableNamespace | null>(null);
    const [namespaceUsers, setNamespaceUsers] = React.useState<NamespaceUser[] | null>(null);
    const reloadForm = React.useCallback(() => {
        setLoaded(false);
        Promise.all([
            serviceProvider.namespaceService.get(props.namespaceId).then(ns => setNamespace(ns)),
            serviceProvider.namespaceService.listNamespaceUsers(props.namespaceId, null).then(users => setNamespaceUsers(users)),
        ]).then(() => setLoaded(true));
    }, [props.namespaceId])
    const saveNamespace = React.useCallback(() => {
        setSavingInProgress(true);
        serviceProvider.namespaceService
            .createOrUpdate(namespace!)
            .finally(() => setSavingInProgress(false))
    }, [namespace]);
    const updateNamespaceName = React.useCallback((event) => {
        setNamespace({ id: props.namespaceId, name: event.target.value })
    }, []);
    React.useEffect(() => {
        reloadForm();
    }, [props.namespaceId])

    if (!loaded) {
        return <Loader active inline="centered" />
    } else {
        if (namespace == null || namespaceUsers == null) {
            throw new Error("Invalid state")
        }
        return <Form>
            <Form.Field>
                <Input defaultValue={namespace.name} onChange={updateNamespaceName} />
            </Form.Field>
            <Form.Field>
                <Dimmer active={savingInProgress} inverted>
                </Dimmer>
                <Table>
                    <Table.Header>
                        <Table.Row>
                            <Table.HeaderCell width={8}>Username</Table.HeaderCell>
                            <Table.HeaderCell width={4}>Role</Table.HeaderCell>
                            <Table.HeaderCell width={1}></Table.HeaderCell>
                        </Table.Row>
                    </Table.Header>
                    <Table.Body>
                        {namespaceUsers.map(user => <NamespaceUserRow namespaceId={namespace.id} user={user} key={user.id} onChange={reloadForm} />)}
                        <AddUserToNamespaceRow namespaceId={namespace.id} onComplete={reloadForm} />
                    </Table.Body>
                </Table>
            </Form.Field>
            <Button onClick={saveNamespace} disabled={savingInProgress}>
                <Icon name="save" />
                Save changes
            </Button>
            <Loader active={savingInProgress} inline />
        </Form >
    }
}

function NamespaceUserRow(props: { namespaceId: string, user: NamespaceUser, onChange: () => void }) {
    const [isDeleting, setIsDeleting] = React.useState(false);
    const [isDeleteRequestActive, setDeleteRequestActive] = React.useState(false);
    const disableDeleting = React.useCallback(() => setIsDeleting(false), []);
    const enableDeleting = React.useCallback(() => setIsDeleting(true), []);
    const serviceProvider = React.useContext(ServiceContext);
    const confirmDeleting = React.useCallback(() => {
        setDeleteRequestActive(true);
        serviceProvider.namespaceService
            .removeUserFromNamespace(props.namespaceId, props.user.id)
            .then(props.onChange)
            .catch((e) => { throw new Error("Faield to remove user from namespace", e); })
            .finally(() => setDeleteRequestActive(false))
    }, [])
    return <Table.Row>
        <Table.Cell>
            {props.user.username}
        </Table.Cell>
        <Table.Cell>
            {props.user.role}
        </Table.Cell>
        <Table.Cell>
            {!isDeleting ?
                <Button color="red" icon="delete user" onClick={enableDeleting} />
                :
                !isDeleteRequestActive ?
                    <ButtonGroup>
                        <Button color="red" icon="delete user" onClick={confirmDeleting} />
                        <Button color="grey" icon="cancel" onClick={disableDeleting} />
                    </ButtonGroup>
                    :
                    <Loader inline active={true} />
            }
        </Table.Cell>
    </Table.Row>
}

function AddUserToNamespaceRow(props: { namespaceId: string, onComplete: () => void }) {
    const [editMode, setEditMode] = React.useState<boolean>(false);
    if (editMode) {
        return <AddUserToNamespaceForm
            namespaceId={props.namespaceId}
            onComplete={(_) => {
                setEditMode(false)
                props.onComplete()
            }}
            onCancel={() => setEditMode(false)}
        />
    } else {
        return <Table.Row>
            <Table.Cell />
            <Table.Cell />
            <Table.Cell width={2}>
                <Button icon="add user" color="green" onClick={() => setEditMode(true)} />
            </Table.Cell>
        </Table.Row>
    }

}

function AddUserToNamespaceForm(props: {
    namespaceId: string,
    onComplete: (_: boolean) => void,
    onCancel: () => void
}) {
    const [savingInProgress, setSavingInProgress] = React.useState(false);
    const [user, setUser] = React.useState<NamespaceUser | null>(null);

    const availableRoles = ["admin", "participant"]

    const onUserModified = React.useCallback(function (user: NamespaceUser | null) {
        setUser(user);
    }, [props.namespaceId, user]);

    const serviceProvider = React.useContext(ServiceContext);
    const onSave = React.useCallback((_) => {
        setSavingInProgress(true);
        serviceProvider.namespaceService
            .addUserToNamespace(props.namespaceId, user!)
            .then(() => props.onComplete(true))
            .finally(() => setSavingInProgress(false))
    }, [user])
    return <Table.Row>
        <Table.Cell>
            <FormField width={4}>
                <UserSelectionField namespaceId={props.namespaceId} user={user} onUserModified={onUserModified} />
            </FormField>
        </Table.Cell>
        <Table.Cell>
            <FormField width={4}>
                <Dropdown selection
                    defaultValue={availableRoles[0]}
                    options={availableRoles.map(role => ({ active: user?.role == role, value: role, text: role, key: role }))} />
            </FormField>
        </Table.Cell>
        <Table.Cell width={2}>
            {!savingInProgress ?
                <ButtonGroup>
                    <Button icon="save" color="blue" disabled={user == null} onClick={onSave} />
                    <Button icon="cancel" color="grey" onClick={props.onCancel} />
                </ButtonGroup>
                :
                <Loader inline active />
            }
        </Table.Cell>
    </Table.Row>
}

function UserSelectionField(props: { namespaceId: string, user: NamespaceUser | null, onUserModified: (user: NamespaceUser | null) => void }) {
    const [user, setUser] = React.useState<NamespaceUser | null>(props.user);
    const onUserModified = React.useCallback((user) => {
        setUser(user);
        props.onUserModified(user);
    }, [])
    const clearSelectedUser = React.useCallback(() => {
        onUserModified(null);
    }, []);
    if (user == null) {
        return <UserSelector namespace={null} onSelect={onUserModified} />
    } else {
        return <>
            <CokeBattleUser username={user.username} />
            <Button icon="delete" color="red" onClick={clearSelectedUser} />
        </>
    }
}

export function NamespaceReference(props: { name: string, namespaceId: string, allowEdit?: boolean }) {
    return <>
        {props.allowEdit ? <Link to={`/namespaces/${props.namespaceId}/edit`}>{props.name}</Link> : props.name}
    </>
}