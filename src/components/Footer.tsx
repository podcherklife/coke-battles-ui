import {
    Container,
    Divider,
    Grid,
    Image,
    Header,
    List,
    Segment,
} from 'semantic-ui-react';
import React from "react";

export default function () {
    return (
        <Segment inverted vertical style={{ margin: '5em 0em 0em', padding: '5em 0em' }}>
            <Container textAlign='center'>
                <Image centered size='mini' src='/logo192.png' />
                <List horizontal inverted divided link size='small'>
                    <List.Item as='a' href='#'>
                        Site Map
                    </List.Item>
                </List>
            </Container>
        </Segment>
    )
}