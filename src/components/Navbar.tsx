import { Container, Image, Menu, Button } from "semantic-ui-react";
import React from "react";
import AuthBlock from './AuthBlock'

export default function Navbar() {
    return (
        <Menu fixed='top' inverted>
            <Container>
                <Menu.Item as='a' href="/" header>
                    <Image size='mini' src='/logo192.png' style={{ marginRight: '1.5em' }} />
                    Waves
                </Menu.Item>
                <Menu.Item position="right">
                    <AuthBlock />
                </Menu.Item>
            </Container>
        </Menu>
    )
}