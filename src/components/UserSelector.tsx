import React from "react";
import { NamespaceUser } from "../services/NamespaceService";
import { ServiceContext } from "./App";
import { Search } from "semantic-ui-react";

export function UserSelector(props: { namespace: string | null, initialText?: string, onSelect: (selectedUser: NamespaceUser) => void }) {
    const [searchResult, setSearchResult] = React.useState<any>([]);
    const [searchValue, setSearchValue] = React.useState(props.initialText);
    const [loading, setLoading] = React.useState(false);
    const [nextRefetchTimeout, setNextRefetchTimeout] = React.useState<NodeJS.Timeout | null>(null);
    const serviceProvider = React.useContext(ServiceContext);
    function refetchUsers(searchString?: string) {
        const namespaceId = props.namespace
        if (namespaceId != null) {
            serviceProvider.namespaceService
                .listNamespaceUsers(namespaceId, searchString || "")
                .then(result => result.map(user => { return { title: user.username, description: user.id, id: user.id, user } }))
                .then(setSearchResult)
                .finally(() => setLoading(false));
        } else {
            serviceProvider.userService
                .list(searchString || "")
                .then(result => result.map(user => { return { title: user.username, description: user.id, id: user.id, user } }))
                .then(setSearchResult)
                .finally(() => setLoading(false))
        }
    }

    function scheduleRefetchUsers(searchString?: string) {
        setLoading(true);
        if (nextRefetchTimeout) {
            clearTimeout(nextRefetchTimeout);
        }
        setNextRefetchTimeout(setTimeout(() => {
            refetchUsers(searchString)
        }, 500));
    }
    React.useEffect(() => scheduleRefetchUsers(searchValue), [searchValue]);
    return <>
        <Search
            loading={loading}
            results={searchResult}
            value={searchValue}
            minCharacters={0}
            onSearchChange={(evt, data) => {
                setSearchValue(data.value || "")
            }}
            onResultSelect={(evt, data) => {
                setSearchValue("");
                props.onSelect(data.result.user as NamespaceUser);
            }}
        ></Search>
    </>;
}