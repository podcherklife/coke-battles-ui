import { useAuth0 } from '@auth0/auth0-react';
import * as React from 'react';
import {
    BrowserRouter as Router,
    Redirect,
    Route, Switch
} from 'react-router-dom';
import {
    Container,
    Header
} from 'semantic-ui-react';
import { config } from "../config";
import { aws, localDynamoDB, noop } from '../services/ServiceProvider';
import { ProfileProvider } from './Auth';
import { CokeBattlesTable as CokeDisputesTable } from './CokeBattlesTable';
import Footer from './Footer';
import { NamespaceEditor } from './NamespaceEditor';
import Navbar from './Navbar';
import { UserNamespaces } from './UserNamespaces';



export const ServiceContext = React.createContext(noop());


function App() {
    const auth = useAuth0();
    return (
        <div>
            {/* <ServiceContext.Provider value={localDynamoDB()}> */}
            <ServiceContext.Provider value={aws(() => auth.getIdTokenClaims().then(token => token!.__raw), config.aws.api)}>
                <ProfileProvider>
                    <Router>
                        <Navbar />
                        <Container style={{ marginTop: '7em' }}>
                            <Switch>
                                <Route exact path="/" render={() => (
                                    <>
                                        <Header as='h1'>Coke Battles</Header>
                                        One day here will be the list of public namespaces...
                                        <UserNamespaces />
                                    </>
                                )} />
                                <Route exact path="/namespaces/:namespace/battles" render={() => (
                                    <Container>
                                        <CokeDisputesTable />
                                    </Container>
                                )} />
                                <Route exact path="/namespaces/:namespace/edit" render={() => (
                                    <Container>
                                        <NamespaceEditor />
                                    </Container>
                                )} />
                                <Route exact path="*" render={() => <>
                                    <Header as='h1'>Oh no!There is no page here!</Header>
                                </>} />
                            </Switch>
                        </Container>
                        <Footer />
                    </Router>
                </ProfileProvider>
            </ServiceContext.Provider>
        </div >

    );
}

export default App;