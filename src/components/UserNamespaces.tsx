import React from "react";
import { Link } from "react-router-dom";
import { Button, ButtonGroup, Container, Dimmer, Form, Label, Loader, Segment, Table } from "semantic-ui-react";
import { Namespace, NamespaceInput } from "../services/NamespaceService";
import { ServiceContext } from "./App";
import { ProfileContext } from "./Auth";

export function UserNamespaces() {
    const [loading, setLoading] = React.useState(false);
    const serviceProvider = React.useContext(ServiceContext);
    const auth = React.useContext(ProfileContext);
    const [userNamespaces, setUserNamespaces] = React.useState<Namespace[]>([]);
    function refreshUserNamespaces() {
        setLoading(true);
        serviceProvider.namespaceService
            .listUserNamespaces(auth.profile!.userId)
            .then(setUserNamespaces)
            .finally(() => setLoading(false))
    }
    React.useEffect(() => {
        auth.isAuthenticated && refreshUserNamespaces();
    }, [auth.profile?.userId]);

    let saveNamespaceAndRefresh = React.useCallback((namespace) => {
        setLoading(true);
        return serviceProvider.namespaceService
            .createOrUpdate(namespace)
            .then(refreshUserNamespaces)
            .finally(() => setLoading(false))
    }, [auth.profile?.userId])

    if (!auth.isAuthenticated) {
        return <>Nope</>
    } else {
        return <Dimmer.Dimmable>
            <Dimmer inverted active={loading} />
            <Loader active={loading} />
            <Table celled>
                <Table.Header>
                    <Table.Row>
                        <Table.HeaderCell width="3">Name</Table.HeaderCell>
                        <Table.HeaderCell width="1">Role</Table.HeaderCell>
                        <Table.HeaderCell width="1"></Table.HeaderCell>
                    </Table.Row>
                </Table.Header>
                <Table.Body>
                    {userNamespaces.map(namespace => <UserNamespaceRow key={namespace.id} namespace={namespace} onSave={saveNamespaceAndRefresh} />)}
                    <AddNamespace onSave={saveNamespaceAndRefresh} />
                </Table.Body>
            </Table>
        </Dimmer.Dimmable>
    }
}

function EditNamespace(props: { namespace: NamespaceInput | null, onSave: (namespace: NamespaceInput) => Promise<void>, onCancel: () => void }) {
    let [namespaceName, setNamespaceName] = React.useState<string>(props.namespace?.name || "");
    return <Table.Row>
        <Table.Cell colSpan="2">
            <Form>
                <Form.Field>
                    <label> Id </label>
                    <Label>
                        {props.namespace?.id || "Will be generated"}
                    </Label>
                </Form.Field>
                <Form.Field>
                    <label> Name </label>
                    <Form.Input defaultValue={props.namespace?.name} type="text" onChange={(_, data) => setNamespaceName(data.value)} />
                </Form.Field>
            </Form>
        </Table.Cell>
        <Table.Cell>
            <ButtonGroup>
                <Button onClick={() => props.onSave({ id: props.namespace?.id, name: namespaceName })} icon="save" positive />
                <Button onClick={props.onCancel} icon="cancel" negative />
            </ButtonGroup>
        </Table.Cell>
    </Table.Row>
}

function AddNamespace(props: { onSave: (namespace: NamespaceInput) => Promise<void> }) {
    let [editing, setEditing] = React.useState(false);
    let enableEditMode = React.useCallback(() => setEditing(true), []);
    let disableEditMode = React.useCallback(() => setEditing(false), []);
    if (editing) {
        return <EditNamespace namespace={null} onSave={(ns) => props.onSave(ns).then(disableEditMode)} onCancel={disableEditMode} />
    } else {
        return <Table.Row>
            <Table.Cell colSpan="3">
                <Button onClick={enableEditMode} icon="add" positive></Button>
            </Table.Cell>
        </Table.Row>
    }
}

function UserNamespaceRow(props: { namespace: Namespace, onSave: (namespace: NamespaceInput) => Promise<void> }) {
    let [editing, setEditing] = React.useState(false);
    let enableEditMode = React.useCallback(() => setEditing(true), []);
    let disableEditMode = React.useCallback(() => setEditing(false), []);
    if (editing) {
        return <EditNamespace namespace={props.namespace} onSave={(ns) => props.onSave(ns).then(disableEditMode)} onCancel={disableEditMode} />
    } else {
        return <Table.Row>
            <Table.Cell>
                <Link to={`/namespaces/${props.namespace.id}/battles`}>
                    {props.namespace.name}
                </Link>
            </Table.Cell>
            <Table.Cell>
                {props.namespace.role}
            </Table.Cell>
            <Table.Cell>
                <Button onClick={enableEditMode} positive icon="edit" />
            </Table.Cell>
        </Table.Row>
    }
}