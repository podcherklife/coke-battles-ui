
import { useAuth0 } from "@auth0/auth0-react";
import * as React from 'react';
import { Button, Icon, Image, Loader } from "semantic-ui-react";
import { config } from '../config';
import { ProfileContext, Profile } from "./Auth";

export default function AuthBlock() {
    const auth = React.useContext(ProfileContext);
    if (auth.isLoading) {
        return <Loader active inline="centered" />
    } else if (auth.profile == null) {
        return <LoginButton onLogin={() => auth.doLogin()} />
    } else {
        return <LogoutButton profile={auth.profile} onLogout={() => auth.doLogout()} />
    }
}

function LoginButton(props: { onLogin: () => void }) {
    return <Button onClick={() => props.onLogin()}>Login</Button>
}


function LogoutButton(props: { profile: Profile, onLogout: () => void }) {
    return <>
        <Image as="img" avatar={true} src={props.profile.picture} />
        <Button onClick={() => props.onLogout()}>
            <Icon name="log out" />Logout as {props.profile!.username}
        </Button>
    </>
}