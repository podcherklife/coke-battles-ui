import reportWebVitals from './reportWebVitals';
import ReactDOM from 'react-dom';
import * as React from 'react';
import './index.css';
import App from './components/App';
import { Auth0Provider } from '@auth0/auth0-react'
import 'semantic-ui-css/semantic.min.css';
import { config } from "./config";


ReactDOM.render(
  <Auth0Provider
    domain={config.zeroAuth.domain}
    clientId={config.zeroAuth.clientId}
    authorizationParams={{
      redirect_uri: window.location.origin
    }}
    cacheLocation="localstorage"
  >
    <App />
  </Auth0Provider>,
  document.getElementById('root')
);


// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();